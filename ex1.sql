CREATE TABLE users (
	id INT PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(255),
	created_at DATE)
	
CREATE TABLE comments (
	id INT PRIMARY KEY AUTO_INCREMENT,
	comment_text TEXT,
	user_id INT,
	photo_id INT,
	created_at DATE,
	FOREIGN KEY (photo_id) REFERENCES photos (id))
	
CREATE TABLE photos (
	id INT PRIMARY KEY AUTO_INCREMENT,
	image_url VARCHAR(255),
	user_id INT,
	created_at DATE,
	FOREIGN KEY (user_id) REFERENCES users (id))
	
CREATE TABLE likes (
	photo_id INT,
	user_id INT,
	PRIMARY KEY (photo_id, user_id),
	FOREIGN KEY (photo_id) REFERENCES photos (id),
	FOREIGN KEY (user_id) REFERENCES users (id))
	
CREATE TABLE tags (
	id INT PRIMARY KEY AUTO_INCREMENT,
	tag_name VARCHAR(255),
	created_at DATE)

CREATE TABLE photo_tags (
	photo_id INT,
	tag_id INT,
	PRIMARY KEY (photo_id, tag_id),
	FOREIGN KEY (tag_id) REFERENCES tags (id),
	FOREIGN KEY (photo_id) REFERENCES photos (id)
	)
	
CREATE TABLE follows(
	follower_id INT,
	followee_id INT,
	created_at DATE,
	PRIMARY KEY (follower_id, followee_id),
	FOREIGN KEY (followee_id) REFERENCES users(id),
	FOREIGN KEY (follower_id) REFERENCES users(id)
)	

-- người sử dụng ứng dụng lâu nhất --
SELECT * FROM users WHERE ORDER BY created_at LIMIT 1

-- tìm 2 ngày trong tuần có lượt đăng kí nhiều nhất ---
SELECT created_at, COUNT(id) as count
FROM users 
GROUP BY created_at 
ORDER BY count DESC
LIMIT 2

-- xác định người dng không hoạt động ---
SELECT users.* 
FROM users 
LEFT JOIN photos ON users.id = photos.user_id
WHERE photos.id IS NULL

-- xác định ảnh có nhiều like nhất và người dùng tạo ra nó ---
SELECT *, count(likes.photo_id) as count
FROM likes
INNER JOIN photos ON likes.photo_id = photos.id
INNER JOIN users ON likes.user_id = users.id
GROUP BY likes.photo_id
ORDER BY count DESC


-- tìm số lượng ảnh trung bình cho mỗi người ---
SELECT 

-- tìm 5 người thường được hashtags ---


-- tìm những người dùng đã thích một bức ảnh ---


